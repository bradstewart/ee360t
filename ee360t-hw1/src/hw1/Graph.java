package hw1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
/**
 * Graph.java
 * EE 360T - Spring 2014
 * EID: bjs2639
 * 			
 */

/**
 * @author Brad Stewart
 */
public class Graph {
	
	private Set<Node> nodes = new HashSet<Node>();	
	private Map<Node, Set<Node>> edges = new HashMap<Node, Set<Node>>();

	// Constructor
	public Graph() {
		
	}
	
	/**
	 * Static inner class for representing a node within the graph
	 * @author Brad Stewart
	 *
	 */
	static class Node {
		String fqn; // fully qualified name for the node
		
		Node(String fqn) {
			this.fqn = fqn;
		}
		
		public String toString() {
			return fqn;
		}
		
		public boolean equals(Object obj) {
			if (!(obj instanceof Node)) return false;
			return fqn.equals(obj.toString());
		}
		
		public int hashCode() {
			return fqn.hashCode();
		}
		// other possible constructors and methods are omitted
	}
	
	
	/**
	 * Add a node to the set of nodes and initialize the node's edge set.
	 * If the node already exists, the method does nothing.
	 * 
	 * @param fqn	Fully-qualified name of the node
	 */
	public void addNode(String fqn) { 
		Node node = new Node( fqn );
		nodes.add( node );
		if ( edges.get( node ) == null ) {
			edges.put( node, new HashSet<Node>() );
		}
	}
	
	/**
	 * Adds the nodes to the set of nodes and adds edge from node fqn1 to node fqn2.
	 * 
	 * @param fqn1		Fully-qualified name of the source node
	 * @param fqn2		Fully-qualified name of the destination node
	 */
	public void addEdge(String fqn1, String fqn2) {
		this.addNode( fqn1 );
		this.addNode( fqn2 );
		edges.get( new Node( fqn1 ) ).add( new Node( fqn2 ) );
	}
	
	/**
	 * Determines if there is a path from node fqn1 to node fqn2.
	 * Uses breadth-first search to traverse the call tree.
	 * 
	 * @param fqn1		Fully-qualified name of the source node
	 * @param fqn2		Fully-qualified name of the destination node
	 * @return			True if a path exists, false if it does not
	 */
	public boolean isReachable(String fqn1, String fqn2) {
		LinkedList<Node> reachableNodes = new LinkedList<Node>();
		HashSet<Node> visitedNodes = new HashSet<Node>();
		
		Node source = new Node( fqn1 );
		Node dest = new Node( fqn2 );
	
		reachableNodes.add( source );
		visitedNodes.add(source );
		
		while ( !reachableNodes.isEmpty() ) {
			Node curr = reachableNodes.pollFirst();
			if ( curr.equals( dest ) ) return true;
			
			for ( Node next : edges.get(curr) ) {
				if ( !visitedNodes.contains( next ) ) {
					visitedNodes.add( next );
					reachableNodes.add( next );
				}
			}
		}

		return false;
		
	}
	
	public Set<Node> getEdges( String fqn ) {
		return edges.get( new Node( fqn ) );
	}
	
	// The directions list the method as both printCFG and printGraph, so this is simply an alias
	public void printCFG() {
		printGraph();
	}
	
	public void printGraph() {
		System.out.println("--- NODES ---");
		for ( Node node : nodes ) {
			System.out.println( node.toString() );
		}
		
		System.out.println("--- EDGES ---");
		for ( Map.Entry<Node, Set<Node>> entry : edges.entrySet() ) {
			for ( Node dest : entry.getValue() ) {
				System.out.println( entry.getKey().toString() + " -> " + dest.toString() );
			}
		}
	}
	

}
