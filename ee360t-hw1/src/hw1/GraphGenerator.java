/**
 * GraphGenerator.java
 * EE 360T - Spring 2014
 * EID: bjs2639
 * 			
 */

package hw1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FileASTRequestor;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;

/**
 * @author Brad Stewart
 *
 */
public class GraphGenerator {
	
	Graph hierarchyGraph = new Graph();
	Graph callGraph = new Graph();

	ASTParser parser = ASTParser.newParser(AST.JLS4);
	List<CompilationUnit> units = new ArrayList<CompilationUnit>();
	List<String> sourcePath = new ArrayList<String>();
	List<String> sourceFile = new ArrayList<String>();
	
	// other possible fields are omitted -- feel free to add them if you need to
	IMethodBinding imb;

	
	public GraphGenerator() {
		parser.setResolveBindings(true);
		parser.setStatementsRecovery(true);
		parser.setBindingsRecovery(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
	}
	
	void createGraph() {
		for(final CompilationUnit cu : units) {
			cu.accept(new ASTVisitor() {
				@Override
				public boolean visit(TypeDeclaration node) {
					// Get current node and add it to the graph
					String fqn = node.resolveBinding().getQualifiedName();
					hierarchyGraph.addNode(fqn);				
					
					// Find this nodes parents (may be an actual class, an object, or an interface)
					ArrayList<String> parentNames = new ArrayList<String>();
					
					if ( node.getSuperclassType() != null ) {
						parentNames.add(node.getSuperclassType().resolveBinding().getQualifiedName());
					} else if ( node instanceof java.lang.Object ) {
						parentNames.add("java.lang.Object");
					} else if ( node.isInterface() ) { // "non-type"
						for ( Object type : node.superInterfaceTypes() ) {
							if ( type instanceof Type ) {
								parentNames.add( ((Type) type).resolveBinding().getQualifiedName() );
							}
						}
					}
					
					for ( String parentName : parentNames ) {
						hierarchyGraph.addEdge( parentName, fqn );	
					}
					
					return super.visit(node);
				}
				
				@Override	
				public boolean visit(MethodDeclaration node) {
					imb = node.resolveBinding();				
					return super.visit(node);
				}		
				
				@Override
				public boolean visit(MethodInvocation node) {

					if (node.resolveMethodBinding() == null) return super.visit(node);
					String qfn = getMethodName( node.resolveMethodBinding() );
					
					callGraph.addEdge( qfn , getMethodName( imb ));
					// Find any overridden methods and add those to the call graph as well
					findOverriddenMethods( imb.getDeclaringClass().getSuperclass(), imb, qfn );

					return super.visit(node);
				}
				
				/**
				 * Recursively find if this method is the same as any in its parent tree
				 * @param superclass
				 * @param method
				 */
				private void findOverriddenMethods( ITypeBinding superclass, IMethodBinding method, String invokedMethod ) {
					if ( superclass == null ) return;
					for ( IMethodBinding parentMethod : superclass.getDeclaredMethods() ) {
						if (method.isSubsignature(parentMethod) && parentMethod.isSubsignature(method)) {
							callGraph.addEdge(invokedMethod, getMethodName(parentMethod));
						}
					}
					
					findOverriddenMethods( superclass.getSuperclass(), method, invokedMethod );
				}
				
				/**
				 * Make a fully-qualified method name with parameter types to 
				 * distinguish between overridden methods. 
				 * @param node
				 * @return
				 */
				private String getMethodName(IMethodBinding node) {
					StringBuilder sb = new StringBuilder();
					sb.append( node.getDeclaringClass().getQualifiedName() );
					sb.append(".");
					sb.append( node.getName() );
					
					sb.append('(');
					for ( ITypeBinding parameter : node.getParameterTypes()) {
						sb.append(parameter.getName());
						sb.append(",");
					}
					sb.replace(sb.lastIndexOf(","), sb.length(), ")");
					return sb.toString();
				}				
				
			});
		}
	}
	
	public void generateASTs(String[] classpathEntries, String[] sourcepathEntries, final String[] sourceFilePaths) {
		
			FileASTRequestor requestor = new FileASTRequestor() {
			public void acceptAST(String sourceFilePath, CompilationUnit ast) {
				units.add(ast);
			}
		};
		parser.setEnvironment(classpathEntries, sourcepathEntries, null, true);
		parser.createASTs(sourceFilePaths, null, new String[0], requestor, null);
	}
	
	public void preParsingProcess(String dir) throws IOException {
		File directory = new File(dir);
		if (!directory.exists() || directory.isFile()) {
			throw new IOException("Cannot find target directory.");
		}
		sourcePath.add(dir);
		
		for (File f : directory.listFiles()) {
			if (f.isFile() && f.getName().endsWith(".java")) {
				sourceFile.add(f.getAbsolutePath());
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		if(args.length != 1) {
			System.out.println("Expect one argument.");
			System.exit(1);
		}
		
		GraphGenerator gg = new GraphGenerator();
		gg.preParsingProcess(args[0]);
		gg.generateASTs(null, (String[]) gg.sourcePath.toArray(new String[0]),
		(String[]) gg.sourceFile.toArray(new String[0]));
		gg.createGraph();
		gg.hierarchyGraph.printGraph();
		gg.callGraph.printGraph();
	}

}
