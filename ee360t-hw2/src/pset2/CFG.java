/**
 * EE 360T - HW 2
 * Spring 2014
 * Brad Stewart (bjs2639)
 */
package pset2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
/**
 * A CFG object has a set of nodes that represent bytecode statements and a set of edges that 
 *   represent the flow of control (branches) among statements. Each node contains:
 *     - an integer that represents the position (bytecode line number) of the statement in the method.
 *     -   reference to the method (an object of class org.apache.bcel.classfile.Method) containing the byte code statement; and
 *     -  a reference to the class (an object of class org.apache.bcel.classfile.JavaClass) that defines the method.
 *     
 * The set of nodes is represented using a java.util.HashSet object, and the set of edges using a java.util.HashMap
 * object, which maps a node to the set of its neighbors. The sets of nodes and edges have values that are
 * consistent, i.e., for any edge, say from node a to node b, both a and b are in the set of nodes. Moreover, for
 * any node, say n, the map maps n to a non-null set, which is empty if the node has no neighbors.
 *
 *
 */
public class CFG {

	Set<Node> nodes = new HashSet<Node>();
	HashMap<Node, Set<Node>> edges = new HashMap<Node, Set<Node>>();
	
	public static class Node implements Comparable<Node> {
		int position;
		Method method;
		JavaClass clazz;
		Node(int p, Method m, JavaClass c) {
			position = p;
			method = m;
			clazz = c;
		}
		
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Node)) return false;
			Node n = (Node)o;
			return (position == n.position) && method.equals(n.method) && clazz.equals(n.clazz);
		}
		
		@Override
		public int hashCode() {
			return position + method.hashCode() + clazz.hashCode();
		}
		
		@Override
		public String toString() {
			return clazz.getClassName() + '.' + method.getName() + method.getSignature() + ": " + position;
		}
		
		@Override
		public int compareTo( Node that ) {
			return Integer.compare(this.position, that.position);
		}
	}
	
	public void addNode(int p, Method m, JavaClass c) {
		addNode(new Node(p, m, c));
	}
	
	private void addNode(Node n) {
		nodes.add(n);
		Set<Node> nbrs = edges.get(n);
		if (nbrs == null) {
			nbrs = new HashSet<Node>();
			edges.put(n, nbrs);
		}
	}
	
	public void addEdge(int p1, Method m1, JavaClass c1, int p2, Method m2, JavaClass c2) {
		Node n1 = new Node(p1, m1, c1);
		Node n2 = new Node(p2, m2, c2);
		addNode(n1);
		addNode(n2);
		Set<Node> nbrs = edges.get(n1);
		nbrs.add(n2);
		edges.put(n1, nbrs);
	}
	
	public void addEdge(int p1, int p2, Method m, JavaClass c) {
		addEdge(p1, m, c, p2, m, c);
	}
	
	@Override
	public String toString() {
		return nodes.size() + " nodes\n" + "nodes: " + nodes + '\n' + "edges: " + edges;
	}
	
	public void printGraph() {
		System.out.println("--- NODES ---");
		
		TreeSet<Node> sortedNodes = new TreeSet<Node>();
		sortedNodes.addAll(nodes);
		
		for ( Node node : sortedNodes ) {
			System.out.println( node.toString() );
		}
		
		System.out.println("--- EDGES ---");
		for ( Map.Entry<Node, Set<Node>> entry : edges.entrySet() ) {
			for ( Node dest : entry.getValue() ) {
				System.out.println( entry.getKey().toString() + " -> " + dest.toString() );
			}
		}
	}
}
