package pset2;

public class D {

	public static void main(String[] a) {
		foo(a);
		bar(a);
	}
	static void foo(String[] a) {
		if (a == null) return;
		bar(a);
	}
	static void bar(String[] a) {}
}

/*
Compiled from "D.java"
public class pset2.D extends java.lang.Object{
public pset2.D();
Code:
0: aload_0
1: invokespecial #8; //Method java/lang/Object."<init>":()V
4: return
public static void main(java.lang.String[]);
Code:
0: aload_0
1: invokestatic #16; //Method foo:([Ljava/lang/String;)V
4: aload_0
5: invokestatic #19; //Method bar:([Ljava/lang/String;)V
8: return
static void foo(java.lang.String[]);
Code:
0: aload_0
1: ifnonnull 5
4: return
5: aload_0
6: invokestatic #19; //Method bar:([Ljava/lang/String;)V
9: return
static void bar(java.lang.String[]);
Code:
0: return
}
*/