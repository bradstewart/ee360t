package pset2;

import java.util.HashMap;
import java.util.Map;

import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.BranchInstruction;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.JSR_W;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.ReturnInstruction;
import org.apache.bcel.generic.Select;

/**
 * EE360T - QUESTION 2.2 - Limitation
 * 
 * The following path through the entire execution of our modal of the program does not exist 
 *   in the control flow of the actual program:
 * 
 * (Sec 1) D.main0 -->  D.foo0 ....  D.fooEXIT  -->  D.main4 -->
 * (Sec 2) D.main5 -->  D.bar0  -->  D.barEXIT  -->  D.foo9  -->  D.fooEXIT  -->  D.main4 --> D.main5  -->
 * (Sec 3) D.bar0  -->  D.barEXIT -->  D.main8 --> D.mainEXIT
 * 
 * Our GraphGenerator simply added edges between nodes, but did not constrain execution order of 
 *   those edges. Since the original program makes multiple calls to D.bar, D.bar must have two
 *   possible return paths. No constrains exist in our CFG model to prevent the infeasible path.
 *   In fact, infinitely many such paths exist in our model as (Sec 2) above creates a loop.
 * 
 */

/**
 *  TODO
 * Complete the implementation of GraphGenerator.createCFG, which returns a CFG object that represents
 *   the control-flow graph for all the methods in the given class. For this part, ignore the edges that
 *   represent method invocations as well as jsr[ w] and *switch instructions.
 * Hint: The class org.apache.bcel.generic.BranchInstruction is a superclass of the classes that represent
 *   branching instructions.
 *
 */
public class GraphGenerator {
	
	CFG cfg;
	JavaClass jc;
	ClassGen cg;
	ConstantPoolGen cpg;
	HashMap<String, MethodGen> methods;

	public CFG createCFG(String className) throws ClassNotFoundException {
		cfg = new CFG();
		jc = Repository.lookupClass(className);
		cg = new ClassGen(jc);
		cpg = cg.getConstantPool();
		methods = new HashMap<String,MethodGen>();
		
		storeMethodNames();
		
		// Loop through the methods
		for (Method m : cg.getMethods()) {								
			MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
			InstructionList il = mg.getInstructionList();				
			InstructionHandle[] handles = il.getInstructionHandles();
			
			// Loop through the instructions
			for (InstructionHandle ih : handles) {
				int position = ih.getPosition();
				cfg.addNode(position, m, jc);
				Instruction inst = ih.getInstruction();
				
				// Add branch nodes except JSR_W and switch
				if ( isAllowed(inst) && isBranch(inst) ) {
					cfg.addEdge(position, getBranchTarget( inst ), m, jc);
				} 
				
				// Invoked methods
				else if ( isInvocation(inst) ) {
					MethodGen invokedMethod = methods.get(buildInvocationFqn(inst));
					cfg.addEdge(position, m, jc, 0, invokedMethod.getMethod(), jc); // Start of a new section of byte code (method body)
					cfg.addEdge(Integer.MIN_VALUE, invokedMethod.getMethod(), jc, ih.getNext().getPosition(), m, jc); // Return edge
				} 
				
				// Other instructions
				if ( !isReturn(inst) && !isInvocation(inst) && hasNext(ih) ) {
					cfg.addEdge(position, ih.getNext().getPosition() , m, jc);
				} 
				
				// Return statements
				else if ( isReturn(inst) ) {
					cfg.addEdge(position, Integer.MIN_VALUE, m, jc);
				}
						
			}
		
		}
		return cfg;
	}
	
	/**
	 * Make a unique name for method invocations which matches that produced
	 *   by the cg and cpg and whatever else becuase I cannot figure out what
	 *   all of those mean.
	 *   
	 * @param i
	 * @return
	 */
	private String buildInvocationFqn( Instruction i ) {
		InvokeInstruction inv = ((InvokeInstruction) i);
		return inv.getMethodName(cpg) + ":" + inv.getSignature(cpg);
	}
	
	/**
	 * Conditional clauses for determining the proper edge type
	 * @param i
	 * @return
	 */
	private boolean isBranch( Instruction i ) {
		return (i instanceof BranchInstruction);
	}
	
	private boolean isReturn( Instruction i ) {
		return (i instanceof ReturnInstruction);
	}
	
	private boolean isInvocation( Instruction i ) {
		return (i instanceof INVOKESTATIC);
	}
	
	private boolean isAllowed( Instruction i  ) {
		return !(i instanceof Select) && !(i instanceof JSR_W);
	}
	
	private boolean isRegular( Instruction i  ) {
		return !isBranch(i) && isReturn(i);
	}
	
	private boolean hasNext( InstructionHandle ih ) {
		return ih.getNext() != null;
	}
	
	private int getBranchTarget( Instruction i ) {
		BranchInstruction bi = (BranchInstruction) i;
		return bi.getTarget().getPosition();
	}
	
	/**
	 * Go get all the method names so we can look up them later when we find a
	 *   method invocation instruction.
	 */
	private void storeMethodNames() {
		for (Method m : cg.getMethods()) {								
			MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
			String fqn = m.getName() + ":" + mg.getSignature();			
			methods.put(fqn, mg);
		}
	}
	
	public static void main(String[] a) throws ClassNotFoundException {
		CFG cfg = new GraphGenerator().createCFG("pset2.D"); // example invocation of createCFG
		cfg.printGraph();
	}

}
