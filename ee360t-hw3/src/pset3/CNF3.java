package pset3;

import java.util.ArrayList;
import java.util.List;

public class CNF3 { // represents a 3-CNF formula
	
	private int numVars; // total number of boolean variables in the formula
	// the variables are v_0, v_1, ..., v_{numVars - 1}
	private List<Clause> clauses;
	
	private boolean isNegated( String variable ) {
		 return variable.contains("!");
	}
	
	private int varNum( String variable ) {
		return Integer.parseInt( variable.replaceAll("\\D", "") );
	}
	
	public CNF3(String formula, int numVars, int numClauses) {
		// String "formula" represents the formula in 3-CNF;
		// "numVars" is the number of variables in the "formula";
		// "numClauses" is the number of clauses in the "formula".
		// assume: numVars >= 1, numClauses >= 1, and the
		// variables are from the set { v0, v1, ..., v9 }
		
		this.numVars = numVars;
		this.clauses = new ArrayList<Clause>();
		
		for ( String clauseString : formula.split("and") ) {
			clauseString = clauseString.trim();
			
			int[] vars = new int[3];
			boolean[] bools = new boolean[3];
			String[] variables = clauseString.split("or");
			
			for ( int i = 0; i < variables.length; i++ ) {
				vars[i] = varNum( variables[i] );
				bools[i] = isNegated( variables[i] );
			}
			
			addClause( vars[0], bools[0], vars[1], bools[1], vars[2], bools[2] );
		}

	}
	
	public int numVars() {
		return numVars;
	}
	
	public int numClauses() {
		return clauses.size();
	}
	
	private void addClause(int v1, boolean n1, int v2, boolean n2, int v3, boolean n3) {
		clauses.add(new Clause(v1, n1, v2, n2, v3, n3));
	}
	
	
	public int countSatisfiedClauses(boolean[] assignment) {
		//precondition: assignment != null && assignment.length == numVars
		int count = 0;
		
		for ( Clause clause : clauses ) {
			if ( ( clause.neg1 ^ assignment[clause.var1] ) || ( clause.neg2 ^ assignment[clause.var2] ) || ( clause.neg3 ^ assignment[clause.var3] ) ) {
				 count++;
			 }
		}
		
		return count;
	}
	
	private static class Clause {
		int var1, var2, var3;
		boolean neg1, neg2, neg3; // "neg" iff the corresponding variable is negated
		
		Clause(int v1, boolean n1, int v2, boolean n2, int v3, boolean n3) {
			var1 = v1; neg1 = n1;
			var2 = v2; neg2 = n2;
			var3 = v3; neg3 = n3;
		}
		
	}
	
}