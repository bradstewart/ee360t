package pset3;

import gov.nasa.jpf.vm.Verify;

/**
 * 
 * @author Brad Stewart (bjs2639)
 * EE360T HW3 - Spring 2014
 *
 */
public class MAX3SATSolver {
	
	CNF3 formula;
	
	private void setup( CNF3 formula ) {
		// Save the max encountered number of satisfied clauses to a "global" variable
		// by using a counter with ID = 1 
		Verify.setCounter(1, 0);
		this.formula = formula;
	}
	
	public void solve(CNF3 formula) {

		setup( formula );

		boolean[] assignment = getAssignment();
		int satisfiedClauses = formula.countSatisfiedClauses(assignment);
		
		// Restrict the execution only to increasing numbers of satisfied clauses
		// using JPF's ignoreIf() - http://babelfish.arc.nasa.gov/trac/jpf/wiki/user/api#no1
		Verify.ignoreIf( satisfiedClauses <= Verify.getCounter(1)  );
		// Update our "global" variable in the counter
		Verify.setCounter(1, satisfiedClauses );
		
		System.out.println( "assignment\t<" + assignmentString( assignment ) + ">\t satisfies " + 
							 satisfiedClauses + " out of " + formula.numClauses() );
	}
	
	/**
	 * Get an an array of booleans used as an assignment for the CNF3.
	 * @param formula
	 * @return
	 */
	private boolean[] getAssignment( ) {
		
		boolean[] assignment = new boolean[formula.numVars()];
		for (int i = 0; i < assignment.length; i++) {
			assignment[i] = Verify.getBoolean();
		}
		return assignment;
	}
	
	/**
	 * Get a formated string of all booleans in a given assignment.
	 * @param assignment
	 * @return
	 */
	private String assignmentString( boolean[] assignment ) {
		
		StringBuilder sb = new StringBuilder();
		String prefix = "";
		
		for ( boolean variable : assignment ) {
			sb.append( prefix );
			sb.append( Boolean.toString( variable ));
			prefix = "\t";  
		}
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		CNF3 formula = new CNF3("(v0 or v1 or v2) and (!v0 or !v1 or v2) and (!v0 or v1 or !v2)", 3, 3);
		MAX3SATSolver m = new MAX3SATSolver();
		m.solve(formula);
		
	}
}