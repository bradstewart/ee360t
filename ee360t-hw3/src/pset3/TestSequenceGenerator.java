package pset3;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import gov.nasa.jpf.vm.Verify;

/**
 * 
 * @author Brad Stewart (BJS2639)
 * EE360T HW3 - Spring 2014
 *
 */
public class TestSequenceGenerator {
	
	static String TAB = "   ";
	static String NL = "\n";
	
	int maxSequence = 0;
	int minSequence = 0;
	int sequenceLength = 0;
	ArrayList<String> methodInvocations = new ArrayList<String>();
	
	
	private void setup( List<MethodDecl> methods ) {
		Verify.resetCounter(0);
		maxSequence = methods.size();
		sequenceLength = Verify.getInt( minSequence, maxSequence );
	}
	
	private int getInt( int max ) {
		return Verify.getInt( minSequence, max - 1 );
	}
	
	private String testCase( String classname ) {		
		return "@Test public void test" + Verify.getCounter(0) + "() {" + NL +
				TAB + classname + " var = new " + classname + "();" + NL +
				invocationSequence() +
				TAB + "assertTrue(var.invocationCount() == " + sequenceLength + ");" + NL +
				"}" + NL;
	}
	
	private String invocationSequence( ) {
		StringBuilder sb = new StringBuilder();
		for (String invocation : methodInvocations) {
			sb.append( TAB + invocation + NL);
		}
		return sb.toString();
	}
	
	
	public void generateAllBoundedSequences(String classname, List<MethodDecl> methods, ValueDomains doms) {
		// precondition: assume all methods in "methods" are declared by class "classname" and
		// "doms" define value domains for all method types that appear in given methods;
		// assume also that "classname" defines an instance method "invocationCount()", which
		// returns the number of methods invoked on "this" since its creation
		
		// postcondition: prints a JUnit test suite to standard console; the suite contains
		// method invocation sequences of length <= methods.size(), such that (1) each method
		// sequence for each parameter value combination is executed and (2) each test case
		// includes a test assertion that validates the result of method "invocationCount()".
		
		// Establish PRECONDITIONS
		setup( methods );
		
		// POSTCONDITION 0
		// Invocation sequence of length <= methods.size(), chosen nondeterministically
		// by JPFs ChoiceGenerator - http://babelfish.arc.nasa.gov/trac/jpf/wiki/devel/choicegenerator
		for ( int i = 0; i < sequenceLength; i++ ) {
			
			StringBuilder invocStr = new StringBuilder();			
			MethodDecl methodDeclaration = methods.get( getInt( maxSequence ) );			
			invocStr.append("var." + methodDeclaration.methodName() + "(");
			
			// POSTCONDITION 1
			// Pair each the current invocation sequence with each of its possible inputs
			String prefix = "";
			for ( String argumentType : methodDeclaration.argumentTypes() ) {
				List<Object> valueDomain = doms.getDomain( argumentType );
				invocStr.append( prefix );
				prefix = ", ";
				invocStr.append( valueDomain.get( getInt( valueDomain.size() ) ) );
			}
			
			invocStr.append(");");
			methodInvocations.add( invocStr.toString() );
			
		}
		
		// POSTCONDITION2
		System.out.println( testCase( classname ) );		
		Verify.incrementCounter(0);
		
	}
	
	public static void main(String[] args) {
		// Test behavior 1
//		MethodDecl m = new MethodDecl("C", "m", new String[]{"int"});
//		List<MethodDecl> methods = new LinkedList<MethodDecl>();
//		methods.add(m);
//		new TestSequenceGenerator().generateAllBoundedSequences("C", methods, new ValueDomains());
		
		// Test behavior 2
		MethodDecl m = new MethodDecl("C", "m", new String[]{"int"});
		MethodDecl n = new MethodDecl("C", "n", new String[]{"java.lang.String", "boolean"});
		List<MethodDecl> methods = new LinkedList<MethodDecl>();
		methods.add(m);
		methods.add(n);
		new TestSequenceGenerator().generateAllBoundedSequences("C", methods, new ValueDomains());

	}
	

}
