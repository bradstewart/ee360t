package pset4;
import gov.nasa.jpf.vm.Verify;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 
 */

/**
 * @author Brad Stewart (bjs2639)
 *
 */
public class BinaryTree {
	int size;
	Node root;
	boolean sizeAccessed = false;
	boolean rootAccessed = false;
	
	static int[] sizeList;
	static Node[] nodeList;
	static Object[] elementDomain;
	
	/*
	 * INSTRUMENT BINARYTREE FIELD ACCESSES FOR NONDETERMINSTIC ACCESS
	 */
	Node root() {
		if ( !rootAccessed ) {
			rootAccessed = true;
			root = getNode();
		}
		return root;
	}
	
	int size() {
		if ( !sizeAccessed ) {
			sizeAccessed = true;
			size = Verify.getIntFromList( sizeList );
		}
		return size;
	}
	
	static class Node {
		Object element;
		Node left, right, parent;
		boolean leftAccessed = false;
		boolean rightAccessed = false;
		boolean parentAccessed = false;
		boolean elementAccessed = false;
		
		/*
		 * INSTRUMENT NODE FIELD ACCESSES FOR NONDETERMINSTIC ACCESS
		 */
		
		Node left() {
			if ( !leftAccessed ) {
				leftAccessed = true;
				left = getNode();
			}
			return left;
		}
		
		Node right() {
			if ( !rightAccessed ) {
				rightAccessed = true;
				right = getNode();
			}
			return right;
		}
		
		Node parent() {
			if ( !parentAccessed ) {
				parentAccessed = true;
				parent = getNode();
			}
			return parent;
		}
		
		Object element() {
			if ( !elementAccessed ) {
				elementAccessed = true;
				element = getElement();
			}
			return element;
		}
		
		
		
		
		@Override
		public String toString() {
			String result = "Null";
			if ( this.element != null ) result = element.toString();
			return result;
		}
	}
	
	public boolean repOk() {
		// precondition: true !No limits on the values of element
		// postcondition: result = true <=> the input is a valid binary tree with parent pointers
		
		// Verify the state of the root node
		if ( root == null && size == 0 ) {
			return true;
		} else if ( root == null && size != 0 ) {
			return false;
		}
		
		if ( root.parent != null ) {
			return false;
		}
		
		Queue<Node> nodes = new LinkedList<Node>();
		nodes.add( root );
		HashSet<Node> visitedNodes = new HashSet<Node>();
		
		while ( !nodes.isEmpty() ) {
			Node currentNode = nodes.poll();
			if ( !visitedNodes.add( currentNode ) ) {
				return false;
			}
			
			// Verify the right child
			if ( currentNode.right != null ) {
				if ( currentNode.right.parent != currentNode ) {
					return false;
				} else {
					nodes.add( currentNode.right );
				}
			}
			
			// Verify the left child
			if ( currentNode.left != null ) {
				if ( currentNode.left.parent != currentNode ) {
					return false;
				} else {
					nodes.add( currentNode.left );
				}
			}
		}
		
		if ( visitedNodes.size() == size ) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public boolean instrumentedRepOk() {
		// precondition: true !No limits on the values of element
		// postcondition: result = true <=> the input is a valid binary tree with parent pointers
		
		// Verify the state of the root node
		if ( root() == null && size() == 0 ) {
			return true;
		} else if ( root() == null && size() != 0 ) {
			return false;
		}
		
		if ( root.parent() != null ) {
			return false;
		}
		
		Queue<Node> nodes = new LinkedList<Node>();
		nodes.add( root() );
		HashSet<Node> visitedNodes = new HashSet<Node>();
		
		while ( !nodes.isEmpty() ) {
			Node currentNode = nodes.poll();
			currentNode.element();
			if ( !visitedNodes.add( currentNode ) ) {
				return false;
			}
			
			// Verify the right child
			if ( currentNode.right() != null ) {
				if ( currentNode.right().parent() != currentNode ) {
					return false;
				} else {
					nodes.add( currentNode.right() );
				}
			}
			
			// Verify the left child
			if ( currentNode.left() != null ) {
				if ( currentNode.left().parent() != currentNode ) {
					return false;
				} else {
					nodes.add( currentNode.left() );
				}
			}
		}
		
		if ( visitedNodes.size() == size() ) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Enumerate all binary trees up to size n then filter invalid trees with repOk().
	 * @param n
	 */
	public void filterBasedGenerator( int n ) {
		// allocate objects
		sizeList = getSizeList( n );
		nodeList = getNodeList( n );	
		
		// set field domains
		elementDomain = getElementDomain();
		
		// assign field values non-deterministically
		root = getNode();
		size = Verify.getIntFromList( sizeList );
		
		for ( Node node : nodeList ) {
			if ( node != null ) {
				node.element = getElement();
				node.parent  = getNode();
				node.left    = getNode();
				node.right   = getNode();
			}
		}
		
		// run repOk to check validity and output the tree if valid
		Verify.ignoreIf( !repOk() );
		System.out.println( toString() );
	}
	
	public void pruningBasedGenerator( int n ) {
		// allocate objects
		sizeList = getSizeList( n );
		nodeList = getNodeList( n );	
		
		// set field domains
		elementDomain = getElementDomain();
		
		// run instrumented repOk to enabled non-deterministic field
		//  assignments on field access, and output the tree if valid.
		Verify.ignoreIf( !instrumentedRepOk() );
		System.out.println(toString());
		
	}
	
	/**
	 * Nondeterministically returns a node from the list of nodes.
	 * @return
	 */
	private static Node getNode() {
		return nodeList[Verify.getInt( 0, nodeList.length-1 )];
	}
	
	private static Object getElement() {
		return elementDomain[Verify.getInt( 0, elementDomain.length-1 )];
	}
	
	/**
	 * Builds a list of all integers from 0 to maxSize - 1.
	 * @param maxSize
	 * @return
	 */
	private int[] getSizeList( int maxSize ) {
		int[] array = new int[maxSize+1];
		
		for ( int i = 0; i <= maxSize; i++ ) {
			array[i] = i;
		}
		
		return array;
	}
	
	/**
	 * Builds a list of possible element values.
	 * @return
	 */
	private Object[] getElementDomain() {
		return new Object[] { null, new Integer(0) };
	}
	
	/**
	 * Builds a list of nodes of length maxSize.
	 * @param maxSize
	 * @return
	 */
	private Node[] getNodeList( int maxSize ) {
		Node[] array = new Node[maxSize+1];
		
		array[0] = null;
		
		for ( int i = 0; i < maxSize; i ++ ) {
			array[i+1] = new Node();
		}
		
		return array;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
        sb.append("[Size: "+ size + " -- ");
        Queue<Node> nodes = new LinkedList<Node>();
        nodes.add( root );
        while ( !nodes.isEmpty() ) {
        	Node current = nodes.poll();
            
            if ( current != null ) {
            	if ( current.element != null ) {
            		sb.append( current.element );
            	} else {
                	sb.append("null");
                }
            	sb.append(' ');
            	nodes.add( current.left );
            	nodes.add( current.right );
            }
        }
        sb.append(']');
        return sb.toString();
	}

	public static void main( String[] args ) {
		BinaryTree b = new BinaryTree();
//		b.size = 2;
		b.pruningBasedGenerator(2);
//		Integer[] nodeList = b.getElementDomain();
//		for ( Integer n : nodeList ) {
//			System.out.println(n);
//		}
		
//		Node[] nodeList = b.getNodeList(2);
//		for ( Node n : nodeList ) {
//			System.out.println(n);
//		}
	}

}
